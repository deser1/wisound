package com.deser1.wisound

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

public class DetailListAdapter(context: Context, mainItems: List<DataServiceOuterClass.Cancion>, detailItems: List<DataServiceOuterClass.Artista>) : BaseAdapter() {

    private val myContext = context
    private val mainItemsList = mainItems
    private val detailItemsList = detailItems

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(myContext)
        val rowMain = layoutInflater. inflate(R.layout.list_item, parent, false)

        val mainItemText = rowMain.findViewById<TextView>(R.id.list_item_song)
        mainItemText.text = mainItemsList[position].nombre

        val detailItemText = rowMain.findViewById<TextView>(R.id.list_item_artist)
        detailItemText.text = detailItemsList[position].nombre

        return rowMain
    }

    override fun getItem(position: Int): Any {
        return position.toLong()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getCount(): Int {
        return mainItemsList.size
    }


}