package com.deser1.wisound

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.deser1.dataservice.DataServiceClient
import com.deser1.dataservice.ExceptionHandler
import com.google.android.material.textfield.TextInputLayout
import io.grpc.StatusRuntimeException


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button_iniciar_sesion)
        button.setOnClickListener{
            iniciarSesion()
        }
    }

    private fun iniciarSesion() {
        val mailInputLayout = findViewById<TextInputLayout>(R.id.mail_text_input)
        val mail = mailInputLayout.editText?.text.toString()

        val passwordInputLayout = findViewById<TextInputLayout>(R.id.password_text_input)
        val password = passwordInputLayout.editText?.text.toString()

        val stub = DataServiceClient()
        try {
            val escucha = stub.iniciarSesion(mail, password)
            (this.application as Session).setEscuchaId(escucha.id)
            val intent = Intent(this, Home::class.java)
            startActivity(intent)
        } catch (e: StatusRuntimeException) {
            val exceptionHandler = ExceptionHandler(e)
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }

    }
}
