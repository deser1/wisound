package com.deser1.wisound

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.deser1.dataservice.DataServiceClient
import com.google.android.material.textfield.TextInputEditText
import io.grpc.StatusRuntimeException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 */
class Search : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_search, container, false)
        val songName = view.findViewById<TextInputEditText>(R.id.search_edit_input)
        val listSongs = view.findViewById<ListView>(R.id.list_results)

        songName.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                listSongs.adapter = null
                try {
                    if (songName.text.toString().isNotEmpty()) {
                        val stub = DataServiceClient()
                        val getSongResponse = stub.searchSong(songName.text.toString())

                        listSongs.adapter = DetailListAdapter(
                            requireContext(),
                            getSongResponse.cancionesList,
                            getSongResponse.artistasList
                        )
                    }
                } catch (e: StatusRuntimeException){
                    Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
        })

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(): Search = Search()
    }
}
