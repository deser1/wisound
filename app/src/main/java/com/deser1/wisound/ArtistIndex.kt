package com.deser1.wisound

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import java.text.FieldPosition

/**
 * A simple [Fragment] subclass.
 * Use the [ArtistIndex.newInstance] factory method to
 * create an instance of this fragment.
 */
class ArtistIndex : Fragment() {

    private val listOptions : ArrayList<String> = arrayListOf<String>(
        "Albums"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_artist_index, container, false)
        val listArtistIndex = view.findViewById<ListView>(R.id.list_artist_index)
        listArtistIndex.adapter = SimpleListAdapter(requireContext(), listOptions)
        listArtistIndex.setOnItemClickListener { parent, view, position, id ->
            openFragment(position)
        }
        return view
    }

    private fun openFragment(position: Int) {
        val fragmentManager = requireActivity().supportFragmentManager

        if (position == 0) {
            val fragmentAlbums = FragmentAlbums.newInstance()
            val fragmentController = FragmentController(fragmentManager, fragmentAlbums)
            fragmentController.openFragment()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() : ArtistIndex = ArtistIndex()
    }
}
