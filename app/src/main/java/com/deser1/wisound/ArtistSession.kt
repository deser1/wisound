package com.deser1.wisound

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.deser1.dataservice.DataServiceClient
import com.deser1.wisound.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class ArtistSession : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_session)

        getArtistId()

        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottomNavigationArtistMenu)
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        val fragmentArtistIndex = ArtistIndex.newInstance()
        openFragment(fragmentArtistIndex)
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        item ->
        when (item.itemId) {
            R.id.buttonNavigationHome -> {
                val fragmentIndex = ArtistIndex.newInstance()
                openFragment(fragmentIndex)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun getArtistId() {
        val stub = DataServiceClient()
        val listenerId = (application as Session).getEscuchaId()
        val artistId = stub.getArtist(listenerId)
        (application as Session).setArtistId(artistId)
    }
}
