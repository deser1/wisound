package com.deser1.wisound

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import androidx.fragment.app.Fragment
import java.text.FieldPosition

/**
 * A simple [Fragment] subclass.
 * Use the [Welcome.newInstance] factory method to
 * create an instance of this fragment.
 */
class Welcome : Fragment() {

    private val listOptions :ArrayList<String> = arrayListOf<String>(
        "Playlists",
        "Biblioteca",
        "Biblioteca local"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_welcome, container, false)
        val listWelcome = view.findViewById<ListView>(R.id.list_welcome)
        listWelcome.adapter = SimpleListAdapter(this.requireContext(), listOptions)
        listWelcome.setOnItemClickListener { parent, view, position, id ->
            openItemFragment(position)
        }
        return view
    }

    private fun openItemFragment(position: Int){
        val fragmentManager = requireActivity().supportFragmentManager
        if (position == 0) {
            val playlist = Playlist()
            val fragmentController = FragmentController(fragmentManager, playlist)
            fragmentController.openFragment()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() : Welcome = Welcome()
    }
}
