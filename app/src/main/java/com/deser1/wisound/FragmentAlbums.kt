package com.deser1.wisound

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import io.grpc.StatusRuntimeException

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentAlbums.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentAlbums : Fragment() {

    var albums: ArrayList<String> = arrayListOf("Crear Album")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_albums, container, false)
        getAlbums()

        val listAlbumsView = view.findViewById<ListView>(R.id.list_albums)
        listAlbumsView.adapter = SimpleListAdapter(requireContext(), albums)
        listAlbumsView.setOnItemClickListener { parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
            openItemFragment(position)
        }

        return view
    }

    private fun getAlbums() {
        val stub = DataServiceClient()
        val idArtist = (requireActivity().application as Session).getArtistId()

        try {
            val albumList = stub.getAlbums(idArtist)

            if (albumList.isNotEmpty()){
                for (album in albumList) {
                    albums.add(album.nombre)
                }
            }
        } catch (e : StatusRuntimeException) {
            Toast.makeText(requireContext(), "Ha ocurrido un error, intente mas tarde", Toast.LENGTH_LONG)
        }
    }

    private fun openItemFragment(position: Int) {
        val fragmentManager = requireActivity().supportFragmentManager

        albums.clear()
        albums.add("CrearAlbum")

        if (position == 0) {
            val albumCreation = FragmentAlbumCreation.newInstance()
            val fragmentController = FragmentController(fragmentManager, albumCreation)
            fragmentController.openFragment()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() : FragmentAlbums = FragmentAlbums()
    }
}
