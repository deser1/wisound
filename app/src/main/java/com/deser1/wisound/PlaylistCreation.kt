package com.deser1.wisound

import android.os.Bundle
import android.renderscript.ScriptGroup
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import com.deser1.dataservice.ExceptionHandler
import com.google.android.material.textfield.TextInputEditText
import io.grpc.StatusRuntimeException
import kotlinx.android.synthetic.*

/**
 * A simple [Fragment] subclass.
 * Use the [PlaylistCreation.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlaylistCreation : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_playlist_creation, container, false)
        val createPlaylistButton = view.findViewById<Button>(R.id.button_crear_playlist)
        createPlaylistButton.setOnClickListener {
            val escuchaId = (requireActivity().application as Session).getEscuchaId()
            val playlistName = view.findViewById<TextInputEditText>(R.id.nombre_playlist_edit_text).text.toString()

            createPlaylist(escuchaId, playlistName)
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() : PlaylistCreation = PlaylistCreation()
    }

    private fun createPlaylist(escuchaId: Int, name: String) {
        try {
            val stub = DataServiceClient()
            val fueRegistrado = stub.createPlaylist(escuchaId, name)
            val fragmentManager = requireActivity().supportFragmentManager
            val playlist = Playlist()
            val fragmentController = FragmentController(fragmentManager, playlist)
            fragmentController.openFragment()
        } catch (e: StatusRuntimeException) {
            val exceptionHandler = ExceptionHandler(e)
            Toast.makeText(this.requireContext(), e.message, Toast.LENGTH_LONG).show()
        }
    }
}
