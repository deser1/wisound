package com.deser1.wisound

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

public class SimpleListAdapter(context: Context, list: ArrayList<String>): BaseAdapter() {
    private val myContext: Context = context
    private val itemList: ArrayList<String> = list

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(myContext)
        val rowMain = layoutInflater.inflate(R.layout.list_simple_item, parent, false)
        val itemName = rowMain.findViewById<TextView>(R.id.list_option)
        itemName.text = itemList.get(position)

        return rowMain
    }

    override fun getItem(position: Int): Any {
        return itemList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return itemList.size
    }

}