package com.deser1.wisound

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import io.grpc.StatusRuntimeException

/**
 * A simple [Fragment] subclass.
 * Use the [Config.newInstance] factory method to
 * create an instance of this fragment.
 */
class Config : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_config, container, false)
        val listConfig = view.findViewById<ListView>(R.id.list_config)
        listConfig.adapter = MyAdapter(this.requireContext())
        listConfig.setOnItemClickListener { parent, view, position, id ->
            openItemFragment(position)
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() : Config = Config()
    }

    private fun openItemFragment(position: Int) {
        val fragmentManager = requireActivity().supportFragmentManager
        try {
            if (position == 1) {
                val stub = DataServiceClient()
                val listenerId = (requireActivity().application as Session).getEscuchaId()
                val artistExists = stub.verifyArtistAccountExists(listenerId)

                if (artistExists) {
                    val intent = Intent(requireActivity(), ArtistSession::class.java)
                    startActivity(intent)
                } else {
                    val termsFragment = TermsFragment.newInstance()
                    val fragmentController = FragmentController(fragmentManager, termsFragment)
                    fragmentController.openFragment()
                }
            }
        } catch (e : StatusRuntimeException) {
            Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG)
        }

    }

    private class MyAdapter(context: Context): BaseAdapter() {
        private val myContext: Context

        private val listConfigurations :ArrayList<String> = arrayListOf<String>(
            "Calidad de stream",
            "Modo creador de contenido"
        )

        private val listConfigurationValue :ArrayList<String> = arrayListOf<String>(
            "256 kps",
            "Desactivado"
        )

        init {
            myContext = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from((myContext))
            val rowMain = layoutInflater.inflate(R.layout.list_config_item, parent, false)

            val configName = rowMain.findViewById<TextView>(R.id.list_item_configuration)
            configName.text = listConfigurations.get(position)

            val valueName = rowMain.findViewById<TextView>(R.id.list_item_configuration_value)
            valueName.text = listConfigurationValue.get(position)

            return rowMain
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return listConfigurations.size
        }

        override fun getItem(position: Int): Any {
            val configName = listConfigurations[position]
            val configValue = listConfigurationValue[position]
            val item :ArrayList<String> = arrayListOf<String>(
                configName,
                configValue
            )
            return item
        }
    }
}
