package com.deser1.wisound

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.io.FileReader

public class FragmentController(fragmentManager: FragmentManager, fragment: Fragment) {
    val fragmentManager: FragmentManager = fragmentManager
    val fragment: Fragment = fragment

    public fun openFragment() {
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}