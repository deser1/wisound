package com.deser1.wisound

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.deser1.dataservice.DataServiceClient
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.listener_menu.*
import kotlin.system.exitProcess

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottomNavigationMenu)
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        val fragmentWelcome = Welcome.newInstance()
        openFragment(fragmentWelcome)
    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { 
        item ->
        when (item.itemId) {
            R.id.buttonNavigationHome -> {
                val welcome = Welcome.newInstance()
                openFragment(welcome)
                return@OnNavigationItemSelectedListener true
            }
            R.id.buttonNavigationSearch -> {
                val search = Search.newInstance()
                openFragment(search)
                return@OnNavigationItemSelectedListener true
            }
            R.id.buttonNavigationConfig -> {
                val config = Config.newInstance()
                openFragment(config)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun BottomNavigationView.checkItem(actionId: Int){
        menu.findItem(actionId)?.isChecked = true
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val fragmentWelcome = supportFragmentManager.findFragmentByTag("fragment_welcome")
            if (fragmentWelcome != null && fragmentWelcome.isVisible){

            }
            else
            {
                val bottomNavigation: BottomNavigationView = findViewById(R.id.bottomNavigationMenu)
                bottomNavigation.checkItem(R.id.buttonNavigationHome)
                val welcome = Welcome.newInstance()
                openFragment(welcome)
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}
