package com.deser1.wisound

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import com.google.android.material.textfield.TextInputEditText
import io.grpc.StatusRuntimeException

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentAlbumCreation.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentAlbumCreation : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_album_creation, container, false)

        val buttonCreateAlbum = view.findViewById<Button>(R.id.button_create_album)
        buttonCreateAlbum.setOnClickListener {
            val idArtist = (requireActivity().application as Session).getArtistId()
            val albumName = view.findViewById<TextInputEditText>(R.id.edit_text_album_name).text.toString()

            createAlbum(idArtist, albumName)
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() : FragmentAlbumCreation = FragmentAlbumCreation()
    }

    private fun createAlbum(idArtist : Int, albumName : String) {
        val stub = DataServiceClient()
        try {
            val registered : Boolean = stub.createAlbum(idArtist, albumName)

            if (registered)
            {
                Toast.makeText(requireContext(), "Album creado", Toast.LENGTH_LONG)

                val fragmentManager = requireActivity().supportFragmentManager
                val fragmentAlbums = FragmentAlbums()
                val fragmentController = FragmentController(fragmentManager, fragmentAlbums)

                fragmentController.openFragment()
            }
        } catch (e: StatusRuntimeException){
            Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG)
        }
    }
}
