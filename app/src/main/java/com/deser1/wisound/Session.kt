package com.deser1.wisound

import android.app.Application

class Session : Application() {

    private var escuchaId: Int = 0
    private var artistId: Int = 0

    fun setArtistId(id: Int) {
        artistId = id
    }

    fun setEscuchaId(id: Int) {
        escuchaId = id
    }

    fun getArtistId() : Int {
        return artistId
    }

    fun getEscuchaId() : Int {
        return escuchaId
    }
}