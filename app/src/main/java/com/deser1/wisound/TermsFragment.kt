package com.deser1.wisound

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import io.grpc.StatusRuntimeException

/**
 * A simple [Fragment] subclass.
 * Use the [TermsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TermsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_terms, container, false)

        val buttonAccept = view.findViewById<Button>(R.id.button_accept)
        buttonAccept.setOnClickListener {

            val stub = DataServiceClient()
            val listenerId = (requireActivity().application as Session).getEscuchaId()
            try{
                val artistExists = stub.verifyArtistAccountExists(listenerId)

                if (artistExists) {
                    val intent : Intent = Intent(requireContext(), ArtistSession::class.java)
                    startActivity(intent)
                } else {
                    val namingArtistFragment = NamingArtist.newInstance()
                    val fragmentManager = requireActivity().supportFragmentManager
                    val fragmentController = FragmentController(fragmentManager, namingArtistFragment)
                    fragmentController.openFragment()
                }

            } catch (e: StatusRuntimeException) {
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
            }
        }

        val buttonCancel = view.findViewById<Button>(R.id.button_cancel)
        buttonCancel.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStackImmediate()
        }

        return view
    }

     companion object {
        @JvmStatic
        fun newInstance() : TermsFragment = TermsFragment()
    }
}
