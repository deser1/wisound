package com.deser1.wisound

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.deser1.dataservice.DataServiceClient
import com.google.android.material.textfield.TextInputEditText

/**
 * A simple [Fragment] subclass.
 * Use the [NamingArtist.newInstance] factory method to
 * create an instance of this fragment.
 */
class NamingArtist : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_naming_artist, container, false)
        val buttonAccept = view.findViewById<Button>(R.id.button_create_artist)
        buttonAccept.setOnClickListener {
            val artistNameInput = view.findViewById<TextInputEditText>(R.id.name_artist_edit_text)

            if (artistNameInput.text.toString().isNotEmpty()) {
                val artistName = artistNameInput.text.toString()
                val stub = DataServiceClient()
                val listenerId = (requireActivity().application as Session).getEscuchaId()

                val artistExists = stub.verifyArtistAccountExists(listenerId)

                if (!artistExists) {
                    val registered = stub.registerArtist(listenerId, artistName)

                    if (registered) {
                        Toast.makeText(requireContext(), "Artista registrado", Toast.LENGTH_LONG).show()
                        val intent = Intent(requireActivity(), ArtistSession::class.java)
                        startActivity(intent)
                    }
                }
            } else {
                Toast.makeText(requireContext(), "Ingrese su nombre artistico", Toast.LENGTH_LONG).show()
            }
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() : NamingArtist = NamingArtist()
    }
}
