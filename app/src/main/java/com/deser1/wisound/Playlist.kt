package com.deser1.wisound

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentManager
import com.deser1.dataservice.DataServiceClient
import kotlinx.coroutines.channels.consumesAll


/**
 * A simple [Fragment] subclass.
 * Use the [Playlist.newInstance] factory method to
 * create an instance of this fragment.
 */
class Playlist : Fragment() {

    var options: ArrayList<String> = arrayListOf("Crear playlist", "Me gusta")
    var playlists: List<DataServiceOuterClass.Playlist> = ArrayList()
    var listViewPlaylists: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val escuchaId = (requireActivity().application as Session).getEscuchaId()

        getPlaylists(escuchaId)

        val view = inflater.inflate(R.layout.fragment_playlist, container, false)

        val optionsList = view.findViewById<ListView>(R.id.list_option)
        optionsList.adapter = SimpleListAdapter(this.requireContext(), options)
        optionsList.setOnItemClickListener { parent, view, position, id ->
            openItemFragment(position)
        }

        val playlistList = view.findViewById<ListView>(R.id.list_playlists)
        playlistList.adapter = PlaylistListAdapter(requireContext(), listViewPlaylists, playlists, requireActivity().supportFragmentManager)

        return view
    }

    private fun openItemFragment(position: Int) {
        val fragmentManager = requireActivity().supportFragmentManager
        if (position == 0) {
            val playlistCreation = PlaylistCreation.newInstance()
            val fragmentController = FragmentController(fragmentManager, playlistCreation)
            fragmentController.openFragment()
        }
    }

    private fun getPlaylists(escuchaId: Int) {
        val stub = DataServiceClient()
        playlists = stub.getPlaylists(escuchaId)
        for (playlist in playlists) {
            listViewPlaylists.add(playlist.nombre)
        }
    }

    class PlaylistListAdapter (context: Context, arrayList: ArrayList<String>, playlists: List<DataServiceOuterClass.Playlist>, fragmentManager: FragmentManager) : BaseAdapter() {
        private val myContext: Context = context
        private val itemList: ArrayList<String> = arrayList
        private val playlists = playlists
        private val fragmentManager = fragmentManager

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(myContext)
            val rowMain = layoutInflater.inflate(R.layout.list_playlist_item, parent, false)
            val itemName = rowMain.findViewById<TextView>(R.id.list_option)
            itemName.text = itemList.get(position)

            val imageButton = rowMain.findViewById<ImageView>(R.id.image_more_options)

            imageButton.setOnClickListener{


                val popupMenu: PopupMenu = PopupMenu(myContext, imageButton)
                popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.option_change_privacy ->
                            changePrivacy(position)
                    }
                    return@OnMenuItemClickListener true
                })
                popupMenu.show()
            }

            return rowMain
        }

        fun changePrivacy(position: Int) {
            val stub = DataServiceClient()
            val updated = stub.changePlaylistPrivacy(playlists[position].id)
            val privacy = playlists[position].privacidad
            if(updated){
                var message = ""
                if (privacy.number == 0){
                   message = "Privacidad: Cerrado"
                } else {
                    message = "Privacidad: Abierto"
                }
                Toast.makeText(myContext, message, Toast.LENGTH_LONG).show()
                val fragmentController = FragmentController(fragmentManager, Playlist.newInstance())
                fragmentController.openFragment()
            }
        }

        override fun getItem(position: Int): Any {
            return itemList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return itemList.size
        }

    }

    companion object {
        @JvmStatic
        fun newInstance() : Playlist = Playlist()
    }
}
