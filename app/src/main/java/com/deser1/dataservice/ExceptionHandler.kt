package com.deser1.dataservice

import io.grpc.Status
import io.grpc.StatusRuntimeException
import java.lang.Exception

class ExceptionHandler : Exception {
    private val statusRuntimeException: StatusRuntimeException
    private val detailMessage: String = ""

    constructor (statusRuntimeException: StatusRuntimeException){
        this.statusRuntimeException = statusRuntimeException
        setMessage()
    }

    private fun setMessage() : String{
        var detailMessage = ""

        detailMessage = when (statusRuntimeException.status.code ) {
            Status.Code.INVALID_ARGUMENT -> statusRuntimeException.message.toString()
            Status.Code.ALREADY_EXISTS -> statusRuntimeException.message.toString()
            else -> "Ha ocurrido un error"
        }

        return detailMessage
    }

    fun getDetailMessage() : String {
        return detailMessage
    }
}