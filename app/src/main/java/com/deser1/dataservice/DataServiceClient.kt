package com.deser1.dataservice

import DataServiceGrpc
import DataServiceOuterClass
import android.text.BoringLayout
import com.deser1.wisound.Playlist
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import java.io.Closeable
import java.util.concurrent.TimeUnit

class DataServiceClient() : Closeable {
    private val host: String = "192.168.1.81"
    private val port: Int = 50051
    private val channel: ManagedChannel
    private val stub: DataServiceGrpc.DataServiceBlockingStub

    init {
        channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build()
        stub =  DataServiceGrpc.newBlockingStub(channel)
    }

    fun iniciarSesion(mail: String, password: String): DataServiceOuterClass.Escucha {
        try {
            val request = DataServiceOuterClass.IniciarSesionRequest.newBuilder().setCorreo(mail)
                .setContrasenia(password).build()
            val escucha = stub.iniciarSesion(request)
            return escucha
        } catch (e: StatusRuntimeException){
            throw e
        }
    }

    fun createPlaylist(escuchaID: Int, nombre: String) : Boolean {
        try {
            var request = DataServiceOuterClass.CrearPlaylistRequest.newBuilder().setEscuchaId(escuchaID).setNombrePlaylist(nombre).build()
            val registrarResponse = stub.crearPlaylist(request)
            return registrarResponse.fueRegistrado
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun getPlaylists(escuchaID: Int) : List<DataServiceOuterClass.Playlist> {
        val getPlaylistReq = DataServiceOuterClass.RecuperarPlaylistsRequest.newBuilder().setEscuchaId(escuchaID).build()
        try {
            val getPlaylistsResponse = stub.recuperarPlaylists(getPlaylistReq)
            return getPlaylistsResponse.playlistsList
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun searchSong(songName: String) : DataServiceOuterClass.RecuperarCancionesResponse {
        val searchSongRequest = DataServiceOuterClass.BuscarCancionRequest.newBuilder().setCancion(songName).build()

        try {
            return stub.buscarCanciones(searchSongRequest)
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun verifyArtistAccountExists(idListener: Int) : Boolean {
        val verifyExistenceReq = DataServiceOuterClass.VerificarExistenciaRequest.newBuilder().setId(idListener).build()

        try {
            val response = stub.verificarExistenciaArtista(verifyExistenceReq)
            return  response.actualizado
        } catch (e: StatusRuntimeException){
            throw e
        }
    }

    fun registerArtist(idListener: Int, name: String) : Boolean {
        val registerArtistReq = DataServiceOuterClass.RegistrarArtistaRequest.newBuilder().setIdEscucha(idListener).setNombre(name).build()

        try {
            val registered = stub.registrarArtista(registerArtistReq)
            return registered.fueRegistrado
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun getArtist(idListener: Int) : Int {
        val escucha = DataServiceOuterClass.Escucha.newBuilder().setId(idListener).build()

        try {
            val artist = stub.recuperarArtista(escucha)
            return artist.id
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun createAlbum(idArtist: Int, albumName: String) : Boolean {
        val registerAlbumRequest = DataServiceOuterClass.RegistrarAlbumRequest.newBuilder().setNombreAlbum(albumName).setIdArtista(idArtist).build()

        try {
            val registered = stub.crearAlbum(registerAlbumRequest)
            return registered.fueRegistrado
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    fun getAlbums(idArtist: Int) : List<DataServiceOuterClass.Album> {
        val artist = DataServiceOuterClass.Artista.newBuilder().setId(idArtist).build()

        try {
            val albumsResponse = stub.recuperarAlbums(artist)
            return albumsResponse.albumsList
        } catch (e : StatusRuntimeException) {
            throw e
        }
    }

    fun changePlaylistPrivacy(id_playlist: Int) : Boolean {
        val playlist = DataServiceOuterClass.Playlist.newBuilder().setId(id_playlist).build()

        try {
            val updateResponse = stub.cambiarPrivacidad(playlist)
            return updateResponse.actualizado
        } catch (e: StatusRuntimeException) {
            throw e
        }
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }
}