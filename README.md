# WiSound

Documento detallado: [https://docs.google.com/document/d/1_kHD0EcxRecsLGgJet11z4oKDBzyeuKFStIgXYgEcQY/edit?usp=sharing](https://docs.google.com/document/d/1_kHD0EcxRecsLGgJet11z4oKDBzyeuKFStIgXYgEcQY/edit?usp=sharing)

## Servidor

Servidor de WiSound: [https://gitlab.com/DelmerLH/wisound-server](https://gitlab.com/DelmerLH/wisound-server)

## Máquina virtual

El servidor de WiSound se ejecuta en maquinas virtuales administradas en Vagrant y ejecutadas con VirtualBox. Link: [https://app.vagrantup.com/delmrlh/boxes/equipo09](https://app.vagrantup.com/delmrlh/boxes/equipo09)


## Colaboradores

*  Delmer López
*  Roberto Quiñones

## Comparación de tecnologías

| Herramienta | Características | Razón para elegirlo |
| ------ | ------ | ----- |
| GitHub | Herramienta de control de cambios que permite activar devops y conectarse a muchas más aplicaciones para complementar funcionalidades | Compatibilidad con otras aplicaciones |
| GitLab | Herramienta de devops que comparte características con azure devops como la creación de wiki y pipelines, además de la funcionalidad de ser el repositorio del proyecto | Experiencia previa con la herramienta |
| Azure devops | Herramienta de devops, permite la creación de pipelines, pero se limita a un número de proyectos para la versión libre. | Se dedicó unas clases a la herramienta en clases. |

## Selección de herramienta

Gitlab. Permite generar pipelines y wikis necesarias para el proyecto de la materia. Comparte características con azure devops, la cuál vimos en clase, y al menos un miembro del equipo está familiarizado con el uso de la herramienta y lleva usándola al menos un año.

## Estrategia de uso de la herramienta

Se utilizará método ágil, lo que implica trabajar por sprints. Cada sprint será una rama a partir de la master que se creará al inicio de cada iteración y al final se unirá de nuevo a la master. Durante el tiempo de vida de la rama del sprint, se crearán ramas para la creación de las funcionalidades, mismas que al ser terminadas pasarán a unirse de nuevo a la rama de sprint.

## Requerimientos funcionales
* RF-01 El sistema deberá reproducir música por streaming
* RF-02 El sistema deberá permitir al escucha crear listas personales de reproducción.
* RF-03 El sistema deberá permitir al escucha cambiar la privacidad de una lista de reproducción.
* RF-04 El sistema deberá permitir al escucha subir un máximo de 250 canciones de la propia biblioteca del usuario.
* RF-05 El sistema deberá permitir buscar canciones.
* RF-06 El sistema deberá permitir al creador de contenido crear albums.
* RF-07 El sistema deberá permitir al creador de contenido agregar canciones a un álbum.
* RF-08 El sistema deberá permitir al creador de contenido modificar albums.
* RF-09 El sistema deberá permitir al escucha calificar canciones con un “me gusta”
* RF-10 Cuando el escucha califica una canción, el sistema deberá agregar dicha canción a la lista de “música que te gusta”.
